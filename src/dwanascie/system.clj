(ns dwanascie.system
  (:require [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [dwanascie.config :as cfg]
            [dwanascie.cli :as cli]
            [dwanascie.tracing :as tracing]
            [dwanascie.metrics :as metrics]
            [dwanascie.logging :as logging]
            [dwanascie.util :as util]))

;;; Core components

(defn- with-info [definition]
  (let [info (select-keys definition [:service/name :service/type :service/version])]
    (update definition :service/components
            #(reduce
               (fn [system name]
                 (assoc-in system [name :service-info] info))
               %
               (keys %)))))

(defn- with-logging [{:keys [service.logging/overrides
                             service.logging/pattern]
                      :as definition}]
  (->> (logging/new-logger overrides pattern)
       (assoc-in definition [:service/components :logging])))

(defn- with-tracing [definition]
  (->> (tracing/new-tracer (:service/name definition))
       (assoc-in definition [:service/components :tracing])))

(defn- with-metrics [definition]
  (let [metrics (->> (map :metrics (vals (:service/components definition)))
                     (reduce concat (:service/metrics definition))
                     set
                     vec)]
    (->> (metrics/new-metrics metrics)
         (assoc-in definition [:service/components :metrics]))))

(defn- with-core-dependencies [system]
  (let [sys-components [:logging :tracing :metrics]
        component-names (remove (set sys-components) (keys system))]
    (component/system-using
      system
      (reduce (fn [deps component-name]
                (assoc deps component-name sys-components))
              {:tracing [:logging]
               :metrics [:logging]}
              component-names))))

(defn- with-defaults [definition config secrets-file]
  (-> definition
      (update :service/version #(or % (util/project-version)))
      (with-logging)
      (with-tracing)
      (with-metrics)
      (with-info)
      (cfg/with-config config secrets-file)))


;;; Setup

(defn- init-exception-logger! []
  (Thread/setDefaultUncaughtExceptionHandler
    (reify Thread$UncaughtExceptionHandler
      (uncaughtException [_ thread ex]
        (log/errorf ex "Uncaught exception on %s" (.getName thread))))))

(defn- check-runtime-environment []
  (let [encoding (System/getProperty "file.encoding")]
    (when-not (= "UTF-8" encoding)
      (throw (ex-info "Please start JVM with property file.encoding set to UTF-8" {:file.encoding encoding})))))


;;; Entry point for repl driven development

(defn- version-info [{:keys [:service/name :service/version]}]
  (str name " " version))

(defn stop [{:keys [definition] :as system}]
  (log/infof "Stopping %s..." (version-info definition))
  (metrics/set-status (:metrics system) :stopping)
  (let [system (component/stop system)]
    (log/infof "Stopped %s" (version-info definition))
    system))

(defn start [definition config-file-or-map secrets-file log-active-config]
  (try
    (let [definition (with-defaults definition config-file-or-map secrets-file)]
      (log/infof "Starting %s..." (version-info definition))
      (when log-active-config
        (log/debugf "Active config:\n%s" (cfg/yaml-config definition)))
      (check-runtime-environment)
      (init-exception-logger!)
      (let [system (-> definition
                       (get :service/components)
                       (component/map->SystemMap)
                       (component/system-using (:service.components/deps definition))
                       (with-core-dependencies)
                       (component/start-system)
                       (assoc :definition definition))]
        (metrics/set-status (:metrics system) :started)
        (log/infof "Started %s" (version-info definition))
        system))
    (catch Throwable t
      (log/fatal t "Failure during system start")
      (when-let [system (:system (ex-data t))]
        (log/warn "Trying to stop partially started system...")
        (stop system)
        (throw t)))))


;;; Entry point for -main

(defn- print-config [definition]
  (println (cfg/yaml-config definition)))

(defn- start-main [definition {:keys [config secrets service-name log-active-config]}]
  (try
    (let [definition  (if service-name
                        (assoc definition :service/name service-name)
                        definition)
          system      (start definition config secrets log-active-config)]
      (.addShutdownHook (Runtime/getRuntime)
                        (Thread. ^Runnable (fn []
                                             (stop system)))))
    (catch Throwable t
      (log/fatal t "Unhandled exception, shutting down...")
      (System/exit 1))))

(defn- print-active-config [definition {:keys [config secrets]}]
  (print-config (with-defaults definition config secrets)))

(defn- print-default-config [definition]
  (print-config (with-defaults definition {} "")))

(def ^:private main-options
  [["-c" "--config FILE" "Path to config file."]
   ["-s" "--secrets FILE" "Path to secrets file."]
   ["-n" "--service-name NAME" "Overwrite service name"]])

(def ^:private default-commands
  {"help"    {:description "Print help"
              :fn          (fn [definition _] (cli/help (with-defaults definition {} "")))}
   "start"   {:description "Start service"
              :options     [[nil "--log-active-config" "Logs active config on service start. Use with caution: logs secret values."]]
              :fn          (fn [definition options] (start-main definition options))}
   "version" {:description "Print version"
              :fn          (fn [definition _] (println (version-info (with-defaults definition {} ""))))}
   "config"  {:description "Manage configuration"
              :subcommands {"active"  {:description "Print active config"
                                       :fn          (fn [definition options] (print-active-config definition options))}
                            "default" {:description "Print default config"
                                       :fn          (fn [definition _] (print-default-config definition))}}}})

(defn main [definition args]
  (-> definition
      (update :service.cli/commands merge default-commands)
      (update :service.cli/options into main-options)
      (update :service.cli/default-command #(or % "start"))
      (cli/run args)))