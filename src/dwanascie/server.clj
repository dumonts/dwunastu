(ns dwanascie.server
  (:require [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [org.httpkit.server :as http-kit]
            [schema.core :as s]))

(defrecord WebServer [stop-fn build-routes config config-shape]

  component/Lifecycle
  (start [this]
    (log/info "Starting Web Server...")
    (let [stop-fn (http-kit/run-server (build-routes this) config)]
      (log/info "Started Web Server")
      (assoc this :stop-fn stop-fn)))

  (stop [this]
    (when stop-fn
      (log/info "Stopping Web Server...")
      (stop-fn))
    (log/info "Stopped Web Server")
    this))

(def default-config
  {:port     8080
   :ip       "0.0.0.0"
   :thread   4
   :max-body 8388608
   :max-line 65536})


(s/defschema WebServerConfig
  {:port     s/Num
   :ip       s/Str
   :thread   s/Num
   :max-body s/Num
   :max-line s/Num})

(defn new-web-server
  [build-routes]
  (map->WebServer {:build-routes build-routes
                   :config-shape WebServerConfig
                   :config       default-config}))
