(ns dwanascie.cli
  (:require [clojure.string :as str]
            [clojure.tools.cli :as cli]))

(defn- error-msg [errors]
  (binding [*out* *err*]
    (println
      (cond (string? errors) errors
            (vector? errors) (str/join \newline errors)))))

(defn- run-fn [definition command arguments main-options]
  (let [{:keys [options errors]} (cli/parse-opts arguments (:options command))
        f (:fn command)]
    (if errors
      (error-msg errors)
      (f definition (into main-options options)))))

(defn- run-command [definition commands arguments main-options]
  (let [default-command (:service.cli/default-command definition)
        command-name    (or (first arguments) default-command)
        command         (get commands command-name)
        command-options (rest arguments)]
    (cond
      (nil? command) (error-msg (format "Subcommand %s not found, available commands are: %s"
                                        command-name (str/join ", " (keys commands))))
      (:subcommands command) (run-command definition (:subcommands command) command-options main-options)
      (:fn command) (run-fn definition command command-options main-options))))

(defn help
  ([definition]
   (printf
     "Service %s version %s

 Options
%s

 Commands
%s

"
     (:service/type definition)
     (:service/version definition)
     (:summary (cli/parse-opts nil (:service.cli/options definition)))
     (str/join (map (partial help definition "  ") (:service.cli/commands definition))))
   (flush))
  ([definition prefix [command-name command]]
   (let [prefix-indent "%-20s"
         new-prefix (str prefix command-name " ")]
     (str
       (when (:fn command)
         (format (str prefix-indent "%s\n")
                 new-prefix
                 (:description command)))
       (str/join (map (partial format (str prefix-indent "%s\n") "")
                      (->> command :options (cli/parse-opts nil) :summary str/split-lines (keep not-empty))))
       (str/join (map (partial help definition new-prefix) (:subcommands command)))))))

(defn run [definition args]
  (let [commands (:service.cli/commands definition)
        options  (:service.cli/options definition)
        {:keys [options arguments errors]} (cli/parse-opts args options :in-order true)]
    (if errors
      (error-msg errors)
      (run-command definition commands arguments options))))