(defproject dwanascie/dwanascie-auth "0.28.3"
  :description "12-factor microservice framework - authentication component with postgreSQL"
  :plugins [[lein-parent "0.3.8"]]
  :parent-project {:path "../../project.clj"
                   :inherit [:managed-dependencies :repositories :manifest :url :license :global-vars]}
  :dependencies [;; dwanascie
                 [dwanascie/dwanascie-core]
                 ;; deps
                 [com.nimbusds/nimbus-jose-jwt "9.9.3"]
                 [metosin/jsonista "0.3.3"]])