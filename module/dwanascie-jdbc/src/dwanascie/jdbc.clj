(ns dwanascie.jdbc
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            [jdbc.core :as jdbc]
            [schema.core :as s])
  (:import com.zaxxer.hikari.HikariDataSource
           java.io.Closeable))

(defn- connection-pool [{:keys [^String jdbc-url ^String jdbc-username ^String jdbc-password] :as cfg}]
  (let [pool (HikariDataSource.)]
    (.setJdbcUrl pool jdbc-url)
    (when (seq jdbc-username)
     (.setUsername pool jdbc-username)
     (.setPassword pool jdbc-password))
    (when (.startsWith jdbc-url "jdbc:tracing:")
      (.setDriverClassName pool "io.opentracing.contrib.jdbc.TracingDriver"))
    (-> (jdbc/connection pool)
        (.close))
    pool))

(defn- db-config [this db-name]
  (-> (:config this)
      (update :jdbc-url str/replace-first "*" db-name)))

(defn ^Closeable connection
  ([jdbc-connection-pool]
   (jdbc/connection (:ds jdbc-connection-pool)))
  ([jdbc-connection-pools db-id]
   (if-let [pool (get @(:pools jdbc-connection-pools) db-id)]
     (jdbc/connection pool)
     (do
       (log/info "Opening JDBC connection pool...")
       (let [new-ds (-> (db-config jdbc-connection-pools db-id)
                        (connection-pool))]
         (log/info "Opened JDBC connection pool")
         (jdbc/connection (-> (swap! (:pools jdbc-connection-pools)
                                     assoc db-id new-ds)
                              (get db-id))))))))

(defrecord JdbcConnectionPool [config ^HikariDataSource ds]

  component/Lifecycle

  (start [this]
    (log/info "Starting JDBC connection pool...")
    (let [new-ds (connection-pool config)]
      (log/info "Started JDBC connection pool")
      (assoc this :ds new-ds)))

  (stop [this]
    (when ds
      (log/info "Stopping JDBC Connection Pool...")
      (.close ^HikariDataSource ds))
    (log/info "Stopped JDBC Connection Pool")
    (assoc this :ds nil)))

(def Jdbc-connection-pool-default-config
  {:jdbc-url      "jdbc:tracing:postgresql://localhost/mydb"
   :jdbc-username ""
   :jdbc-password ""})

(s/defschema JdbcConnectionPoolConfig
  {:jdbc-url      s/Str
   :jdbc-username s/Str
   :jdbc-password s/Str})

(defn new-jdbc-connection-pool []
  (map->JdbcConnectionPool {:config-shape JdbcConnectionPoolConfig
                            :config       Jdbc-connection-pool-default-config}))

(defrecord JdbcConnectionPools [config pools]

  component/Lifecycle

  (start [this]
    (log/info "Started JDBC Connection Pool manager")
    (assoc this :pools (atom {})))

  (stop [this]
    (when pools
      (log/info "Stopping JDBC Connection Pool manager...")
      (doseq [ds (vals @pools)]
        (when ds
          (.close ^HikariDataSource ds))))
    (log/info "Stopped JDBC Connection Pool manager")
    (assoc this :pools nil)))

(def Jdbc-connection-pools-default-config
  {:jdbc-url      "jdbc:tracing:postgresql://localhost/*"
   :jdbc-username ""
   :jdbc-password ""})

(s/defschema JdbcConnectionPoolsConfig
  {:jdbc-url s/Str
   :jdbc-username s/Str
   :jdbc-password s/Str})

(defn new-jdbc-connection-pools []
  (map->JdbcConnectionPools {:config-shape JdbcConnectionPoolsConfig
                             :config       Jdbc-connection-pools-default-config}))