(defproject dwanascie/dwanascie-jdbc "0.28.2"
  :description "12-factor microservice framework - (postgresql) jdbc connection pool component"
  :plugins [[lein-parent "0.3.8"]
            [com.roomkey/lein-v "7.2.0"]]
  :parent-project {:path "../../project.clj"
                   :inherit [:managed-dependencies :repositories :manifest :url :license :global-vars]}
  :dependencies [;; dwanascie
                 [dwanascie/dwanascie-core]
                 ;; deps
                 [com.layerware/hugsql-adapter-clojure-jdbc "0.5.1"]
                 [com.layerware/hugsql-core "0.5.1"]
                 [com.zaxxer/HikariCP "4.0.3" :exclusions [org.slf4j/slf4j-api]]
                 [funcool/clojure.jdbc "0.9.0"]
                 [io.opentracing.contrib/opentracing-jdbc "0.2.15"]
                 [org.postgresql/postgresql "42.2.21"]])